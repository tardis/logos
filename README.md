# Logos

Contains logos in illustrator format, with some prerenders. 

The font is Roboto Slab, and main colours are:
  - `#285084` / `rgb(40, 80, 132)`
  - `#1b3558` / `rgb(27, 53, 88)`
  - `#f4ff5c` / `rgb(244, 255, 92)`